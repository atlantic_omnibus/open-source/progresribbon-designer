<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ ProgressRibbon Designer v1.0
  ~
  ~ Copyright (c) 2019 Attila Orosz
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  ~ associated documentation files (the "Software"), to deal in the Software without restriction,
  ~ including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  ~ and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
  ~ so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all copies or substantial
  ~ portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  ~ INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  ~ PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ~ ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
  ~ THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  -->

<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/publisher_info_header"
            style="@style/aboutPageHeaderStyle"
            android:onClick="onAboutHeaderClick">
            <TextView
                android:id="@+id/pub_info_label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/publisher_header"
                android:textSize="16sp"
                android:textColor="@color/colorPrimaryDark"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toStartOf="@id/pub_info_chevron"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_chainStyle="spread_inside"/>
            <ImageView
                android:id="@+id/pub_info_chevron"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:src="@drawable/ic_expand_more_18dp"
                android:rotation="180"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toEndOf="@+id/pub_info_label"/>
        </androidx.constraintlayout.widget.ConstraintLayout>

        <net.cachapa.expandablelayout.ExpandableLayout
            android:id="@+id/pub_info_expandable_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:el_expanded="true"
            app:layout_constraintTop_toBottomOf="@id/publisher_info_header">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="24dp">

                <ImageView
                    android:id="@+id/dev_logo"
                    android:layout_width="150dp"
                    android:layout_height="150dp"
                    android:layout_gravity="center_horizontal"
                    android:layout_marginBottom="24dp"
                    android:src="@drawable/atlantic_omnibus"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"/>
                <TextView
                    android:id="@+id/publisher_text"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/publisher_description"
                    android:textStyle="italic"
                    android:layout_marginTop="24dp"
                    android:textSize="17sp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/dev_logo" />
                <ImageView
                    android:id="@+id/gitlab_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_gitlab_icon_48dp"
                    android:layout_marginTop="36dp"
                    app:layout_constraintEnd_toStartOf="@+id/github_logo"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/publisher_text"
                    app:layout_constraintHorizontal_chainStyle="spread"
                    android:tag="@string/omnibus_gitlab_tag"
                    android:onClick="onSocialIconClick"/>
                <ImageView
                    android:id="@+id/github_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_github_icon_48dp"
                    app:layout_constraintBottom_toBottomOf="@+id/gitlab_logo"
                    app:layout_constraintEnd_toStartOf="@id/playstore_logo"
                    app:layout_constraintStart_toEndOf="@id/gitlab_logo"
                    app:layout_constraintTop_toTopOf="@id/gitlab_logo"
                    android:tag="@string/omnibus_github_tag"
                    android:onClick="onSocialIconClick"/>
                <ImageView
                    android:id="@+id/playstore_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:alpha="0.2"
                    android:src="@drawable/ic_playstore_48dp"
                    app:layout_constraintEnd_toStartOf="@id/email_logo"
                    app:layout_constraintStart_toEndOf="@id/github_logo"
                    app:layout_constraintTop_toTopOf="@id/github_logo"
                    app:layout_constraintBottom_toBottomOf="@+id/github_logo"
                    android:onClick="onSocialIconClick"
                    />
                <ImageView
                    android:id="@+id/email_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_mail_outline_48dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/playstore_logo"
                    app:layout_constraintTop_toTopOf="@id/playstore_logo"
                    app:layout_constraintBottom_toBottomOf="@+id/playstore_logo"
                    android:tag="@string/omnibus_email_tag"
                    android:onClick="onSocialIconClick"/>
            </androidx.constraintlayout.widget.ConstraintLayout>
        </net.cachapa.expandablelayout.ExpandableLayout>

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/developer_info_header"
            style="@style/aboutPageHeaderStyle"
            android:onClick="onAboutHeaderClick">
            <TextView
                android:id="@+id/dev_info_label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/developer_header"
                android:textSize="16sp"
                android:textColor="@color/colorPrimaryDark"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toStartOf="@id/dev_info_chevron"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_chainStyle="spread_inside"/>
            <ImageView
                android:id="@+id/dev_info_chevron"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:src="@drawable/ic_expand_more_18dp"

                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toEndOf="@+id/dev_info_label"/>
        </androidx.constraintlayout.widget.ConstraintLayout>
        <net.cachapa.expandablelayout.ExpandableLayout
            android:id="@+id/dev_info_expandable_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:el_expanded="false">
            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="24dp">
                <de.hdodenhof.circleimageview.CircleImageView
                    android:id="@+id/dev_info_image"
                    android:layout_width="82dp"
                    android:layout_height="82dp"
                    android:layout_marginTop="24dp"
                    android:src="@drawable/lead_dev"
                    app:layout_constraintTop_toTopOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintBottom_toTopOf="@id/dev_info_textview"
                    app:layout_constraintVertical_bias="0"/>
                <TextView
                    android:id="@+id/dev_name_textview"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="16dp"
                    android:textSize="18sp"
                    android:textStyle="bold"
                    android:text="@string/lead_dev_name"
                    android:textColor="@android:color/black"
                    app:layout_constraintTop_toBottomOf="@id/dev_info_image"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintBottom_toTopOf="@id/dev_info_textview"/>
                <TextView
                    android:id="@+id/dev_info_textview"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="24dp"
                    android:gravity="center"
                    android:text="@string/lead_dev_bio"
                    app:layout_constraintTop_toBottomOf="@id/dev_name_textview"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent" />
                <ImageView
                    android:id="@+id/dev_gitlab_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_gitlab_icon_48dp"
                    android:layout_marginTop="36dp"
                    app:layout_constraintEnd_toStartOf="@+id/dev_github_logo"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/dev_info_textview"
                    app:layout_constraintHorizontal_chainStyle="spread"
                    android:tag="@string/developer_gitlab_tag"
                    android:onClick="onSocialIconClick"/>
                <ImageView
                    android:id="@+id/dev_github_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_github_icon_48dp"
                    app:layout_constraintBottom_toBottomOf="@+id/dev_gitlab_logo"
                    app:layout_constraintEnd_toStartOf="@id/dev_email_logo"
                    app:layout_constraintStart_toEndOf="@id/dev_gitlab_logo"
                    app:layout_constraintTop_toTopOf="@id/dev_gitlab_logo"
                    android:tag="@string/developer_github_tag"
                    android:onClick="onSocialIconClick" />
                 <ImageView
                    android:id="@+id/dev_email_logo"
                    android:layout_width="24dp"
                    android:layout_height="24dp"
                    android:src="@drawable/ic_mail_outline_48dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/dev_github_logo"
                    app:layout_constraintTop_toTopOf="@id/dev_github_logo"
                    app:layout_constraintBottom_toBottomOf="@+id/dev_github_logo"
                    android:tag="@string/developer_email_tag"
                    android:onClick="onSocialIconClick" />
            </androidx.constraintlayout.widget.ConstraintLayout>
        </net.cachapa.expandablelayout.ExpandableLayout>
        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/libraries_info_header"
            style="@style/aboutPageHeaderStyle"
            android:onClick="onAboutHeaderClick">
            <TextView
                android:id="@+id/libs_info_label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/libraries_header"
                android:textSize="16sp"
                android:textColor="@color/colorPrimaryDark"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toStartOf="@id/libs_info_chevron"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_chainStyle="spread_inside"/>
            <ImageView
                android:id="@+id/libs_info_chevron"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:src="@drawable/ic_expand_more_18dp"

                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toEndOf="@+id/libs_info_label"/>
        </androidx.constraintlayout.widget.ConstraintLayout>
        <net.cachapa.expandablelayout.ExpandableLayout
            android:id="@+id/libs_info_expandable_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:el_expanded="false"
            android:padding="16dp">
            <TextView
                android:id="@+id/libs_info_textview"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"/>
        </net.cachapa.expandablelayout.ExpandableLayout>
        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/licenses_info_header"
            style="@style/aboutPageHeaderStyle"
            android:onClick="onAboutHeaderClick">
            <TextView
                android:id="@+id/license_info_label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/licenses_header"
                android:textSize="16sp"
                android:textColor="@color/colorPrimaryDark"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toStartOf="@id/license_info_chevron"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_chainStyle="spread_inside"/>
            <ImageView
                android:id="@+id/license_info_chevron"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:src="@drawable/ic_expand_more_18dp"

                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toEndOf="@+id/license_info_label"/>
        </androidx.constraintlayout.widget.ConstraintLayout>
        <net.cachapa.expandablelayout.ExpandableLayout
            android:id="@+id/license_info_expandable_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:el_expanded="false"
            android:padding="16dp">

            <TextView
                android:id="@+id/license_info_textview"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"/>
        </net.cachapa.expandablelayout.ExpandableLayout>


        <!--View to pad out bottom, so that the last header can keep its elevation when
        its ExpandableLayout is collapsed-->
        <View
            android:layout_width="match_parent"
            android:layout_height="3dp"/>

    </LinearLayout>
</ScrollView>
