/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.core;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.atlanticomnibus.controlsheet.ControlSheet;
import com.atlanticomnibus.progressribbon.ProgressRibbon;
import com.atlanticomnibus.ribbondesigner.R;
import com.atlanticomnibus.switchbutton.SwitchButton;


/**
 * A collection of static methods to handle the ProgressRibbon
 */
class RibbonControl {

    /**
     * Will call createRibbon in a way that will only reset the existing one.
     * @param activity Recerence to the Activity craeating the ribbon. Ribbon needs ths
     * @param progressRibbon The old ProgressRibbn. We might not want to destrioy it, might just be resettnig it
     * @param sheetControlPager The ControlSheet to get the buttons from
     * @return the same Ribbon with new values applied
     */
    public static ProgressRibbon resetRibbon(Activity activity, ProgressRibbon progressRibbon, ViewPager sheetControlPager){
        return manageRibbon(activity, progressRibbon, null, sheetControlPager, true);
    }

    /**
     * Sorter way to call createRibbon, which will alsway create a new Ribbon (no resettng invoolved
     * @param activity Recerence to the Activity craeating the ribbon. Ribbon needs ths
     * @param progressRibbon The old ProgressRibbn. We might not want to destrioy it, might just be resettnig it
     * @param ribbonContainer RibbonContainer ViewGroup, in casre th rRibbon is not orphan. if null, the new Ribbon owuld be an orphan
     * @param sheetControlPager The ControlSheet to get the buttons from
     * @return the newly created Ribbon
     */
    public static ProgressRibbon createRibbon(Activity activity, ProgressRibbon progressRibbon, ViewGroup ribbonContainer, ViewPager sheetControlPager){
        return manageRibbon(activity, progressRibbon, ribbonContainer, sheetControlPager, false);
    }


    /**
     * Will recreate the ProgressRibbon and return the new one. It checks the sheetControlPager's control and applies their current state to the Ribbon.
     * It does not currently care if the values might be default, but you should. (Method doesn't so you can see the code in action, heh.)
     *
     * @param activity Recerence to the Activity craeating the ribbon. Ribbon needs ths
     * @param progressRibbon The old ProgressRibbn. We might not want to destrioy it, might just be resettnig it
     * @param ribbonContainer RibbonContainer ViewGroup, in casre th rRibbon is not orphan. if null, the new Ribbon owuld be an orphan
     * @param sheetControlPager The ControlSheet to get the buttons from
     * @param resetOnly If true, the Ribbon will not be recreated only reset to whatever values the ControlSheet currently has (should be defaults)
     * @return the new, or altered ProgressRibbon
     */
    private static ProgressRibbon manageRibbon(Activity activity, ProgressRibbon progressRibbon, ViewGroup ribbonContainer, ViewPager sheetControlPager, boolean resetOnly){

        boolean orphanModeToggleChecked =  ((SwitchButton)sheetControlPager.findViewById(R.id.orphan_mode_toggle)).isChecked();
        boolean barStyleToggleChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.bar_indicator_toggle)).isChecked();
        boolean setViewBlockingChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.block_view_toggle)).isChecked();
        boolean indeterminateToggleChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.indeterminate_toggle)).isChecked();
        boolean textBesideBarChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.text_beside_spinner_toggle)).isChecked();
        boolean dialogueModeChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.dialogue_mode_toggle)).isChecked();
        boolean progressAsPercentChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.result_as_percentage_toggle)).isChecked();
        boolean transparentRibbonChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.ribbon_transparent_toggle)).isChecked();
        boolean marginAsPercentChecked = ((SwitchButton)sheetControlPager.findViewById(R.id.margin_as_percent_toggle)).isChecked();

        int animationTypeValue = ((Spinner)sheetControlPager.findViewById(R.id.animation_type_chooser)).getSelectedItemPosition();
        int minValue = Integer.parseInt(((TextView)sheetControlPager.findViewById(R.id.ribbon_min_value)).getText().toString());
        int maxValue = Integer.parseInt(((TextView)sheetControlPager.findViewById(R.id.ribbon_max_value)).getText().toString());
        int showDelayValue = Integer.parseInt(((TextView)sheetControlPager.findViewById(R.id.ribbon_show_delay_value)).getText().toString());
        int hideDelayValue = Integer.parseInt(((TextView)sheetControlPager.findViewById(R.id.ribbon_hide_delay_value)).getText().toString());
        int topPaddingValue = Integer.parseInt(((TextView)sheetControlPager.findViewById(R.id.ribbon_padding_top_value)).getText().toString());
        int bottomPaddingValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_padding_bottom_value)).getText().toString());
        int animationDurationValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.animation_duration_value)).getText().toString());
        int borderRadiusValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_border_radius_value)).getText().toString());
        int borderSizeValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_border_size_value)).getText().toString());
        int textColorValue = Color.parseColor("#" + ((EditText)sheetControlPager.findViewById(R.id.ribbon_text_color_value)).getText().toString());
        int textSizeValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_text_size_value)).getText().toString());
        int borderColorValue = Color.parseColor("#" + ((EditText)sheetControlPager.findViewById(R.id.ribbon_border_color_value)).getText().toString());
        int backgroundColorValue = Color.parseColor("#" + ((EditText)sheetControlPager.findViewById(R.id.ribbon_background_color_value)).getText().toString());
        int elevationValue = Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_elevation_value)).getText().toString());
        int margiTopValue=Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_margin_top_value)).getText().toString());
        int marginBottomValue=Integer.parseInt(((EditText)sheetControlPager.findViewById(R.id.ribbon_margin_bottom_value)).getText().toString());


        // This is only to demonstrate the different working modes (orphan nad view attached).
        // You would not normally need o recreate the Ribbon,unless you have a specific reason for it
        if(progressRibbon!=null) {
            if(!resetOnly) {
                progressRibbon.removeDynamicRibbon();
                progressRibbon = orphanModeToggleChecked ? new ProgressRibbon(activity) : new ProgressRibbon(activity, ribbonContainer);
            }
        } else {
            progressRibbon = orphanModeToggleChecked ? new ProgressRibbon(activity) : new ProgressRibbon(activity, ribbonContainer);
        }


        // The second listener never ges called, because the main listener which normally calls the
        // other isteners is overridden. uncomment the first istener to get access to the second
        // (and all other smaller listeners)
        progressRibbon.setOnRibbonStateChangedListener(onRibbonStateChangeListener);
        progressRibbon.setOnRibbonShowListener(showListener);

        progressRibbon.setProgressBarStyle(barStyleToggleChecked ? ProgressRibbon.BAR_HORIZONTAL : ProgressRibbon.BAR_ROUND)
                      .setViewBlocking(setViewBlockingChecked)
                      .setIndeterminateState( indeterminateToggleChecked ? ProgressRibbon.INDETERMINATE : ProgressRibbon.DETERMINATE)
                      .setAnimationType(animationTypeValue)
                      .setRibbonTextPosition(textBesideBarChecked ? ProgressRibbon.TEXT_BESIDE_BAR : ProgressRibbon.TEXT_UNDER_BAR)
                      .setMin(minValue)
                      .setMax(maxValue)
                      .setShowDelay(showDelayValue)
                      .setHideDelay(hideDelayValue)
                      .setRibbonPaddingTop(TypedValue.COMPLEX_UNIT_DIP, topPaddingValue)
                      .setRibbonPaddingBottom(TypedValue.COMPLEX_UNIT_DIP, bottomPaddingValue)
                      .setAnimationDuration(animationDurationValue)
                      .setProgressText(progressRibbon.isIndeterminate() ? R.string.infinite_progress : R.string.progress_ready)
                      .setRibbonInDialogueMode(dialogueModeChecked)
                      .setRibbonBorderRadius(borderRadiusValue)
                      .setRibbonBorderSize(borderSizeValue)
                      .setProgressTextColour(textColorValue)
                      .setProgressTextSize(textSizeValue)
                      .setReportProgressAsMaxPercent(progressAsPercentChecked);

        if(transparentRibbonChecked) {
            progressRibbon.setRibbonTransparent();
        } else {
            progressRibbon.setRibbonBorderColor(borderColorValue)
                          .setRibbonBackgroundColor(backgroundColorValue)
                          .setRibbonElevation(elevationValue);
        }

        if(marginAsPercentChecked){
            progressRibbon.setRibbonMarginTop(ProgressRibbon.PARENT_HEIGHT_PERCENT, margiTopValue)
                          .setRibbonMarginBottom(ProgressRibbon.PARENT_HEIGHT_PERCENT, marginBottomValue);
        } else {
            progressRibbon.setRibbonMarginTop(TypedValue.COMPLEX_UNIT_DIP, margiTopValue)
                          .setRibbonMarginBottom(TypedValue.COMPLEX_UNIT_DIP, marginBottomValue);
        }

        return progressRibbon;
    }







    /****************Some listeners, just to see what they do**************************************/


    /**
     * Attaching this listener nulls all other listeners, but has their functions in one place
     * in the same format.
     *
     * If you use this listener in the app, some functionality will be lost, as the app itself uses other listeners
     */
    private static final ProgressRibbon.RibbonStateChangeListener onRibbonStateChangeListener = new ProgressRibbon.RibbonStateChangeListener() {

        private final String TAG="StatusChangeListener";

        @Override
        public void onRibbonSignalledToShow(int showDelay) {
            Log.d(TAG, "Signalled to show with delay: " + showDelay);
        }

        @Override
        public void onRibbonShow() {
            Log.d(TAG, "Showing now");
        }

        @Override
        public void onRibbonIndeterminateStatusChanged(boolean ribbonIsIndeternimate) {
            Log.d(TAG, "Imndeterminate status changed  to: " + ribbonIsIndeternimate);
        }

        @Override
        public void onRibbonProgressStarted(int startValue) {
            Log.d(TAG, "Progress started at: " + startValue);
        }

        @Override
        public void onRibbonProgressStopped(int finalValue) {
            Log.d(TAG, "Progress stopped at: " + finalValue);
        }

        @Override
        public void onRibbonProgressChange(int currentValue) {
            Log.d(TAG, "Progress updated to " + currentValue);
        }

        @Override
        public void onRibbonSignalledToHide(int hideDelay) {
            Log.d(TAG, "Siglalled to hide with delay: " + hideDelay);
        }

        @Override
        public void onRibbonHide() {
            Log.d(TAG, "Ribbon now hidden");
        }

        @Override
        public void onRibbonAttached(boolean hasViewParent) {
            Log.d(TAG, "Ribbon attached to view or window. Has viewParent: " + hasViewParent);
        }

        @Override
        public void onRibbonRemoved() {
            Log.d(TAG, "Removed ribbon completely");
        }
    };

    /**
     * This would normally be fired from inside the ProgressRibbon class by its own internal
     * RibbonStateChangeListener, unless you override it
     */
    private static final ProgressRibbon.OnRibbonShowListener showListener = new ProgressRibbon.OnRibbonShowListener() {

        private final String TAG="OnRibbonShowListener";

        @Override
        public void onRibbonSignalledToShow(int showDelay) {
            Log.d(TAG, "Ribbon signalled to show with delay: " + showDelay);
        }

        @Override
        public void onRibbonShow() {
            Log.d(TAG, "Now showing ribbon");
        }
    };

}
