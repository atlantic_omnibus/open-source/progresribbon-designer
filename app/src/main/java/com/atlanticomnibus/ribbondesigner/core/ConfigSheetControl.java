/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.core;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.atlanticomnibus.controlsheet.ControlSheet;
import com.atlanticomnibus.controlsheet.ControlSheetInflatedListener;
import com.atlanticomnibus.progressribbon.ProgressRibbon;
import com.atlanticomnibus.ribbondesigner.R;
import com.atlanticomnibus.ribbondesigner.simulation.SimulateProgressAsync;
import com.atlanticomnibus.ribbondesigner.ui.configsheet.TextDrawable;
import com.atlanticomnibus.ribbondesigner.ui.main.MockupScrollerAdapter;
import com.atlanticomnibus.ribbondesigner.ui.ribbon.RibbonViewModel;
import com.atlanticomnibus.switchbutton.SwitchButton;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

/**
 * This is where (most of) the mngic happens
 *
 */
public class ConfigSheetControl implements ControlSheetInflatedListener {


    private final float DISPLAY_DENSITY;
    private final float DISPLAY_SCALED_DENSITY;
    private final int RESET=0, EVALUATE=1;

    private AppCompatActivity mActivity;
    private ProgressRibbon progressRibbon;
    private ViewGroup ribbonContainer;
    private ControlSheet controlSheet;

    public Spinner animationModeChooser;

    // These will serve as decorations for EditTexts where units are needed.
    // These two are needed in multiple places
    private TextDrawable dpDrawable,
                         percentDrawable;

    private InputMethodManager imm;

    // Helper Lists to quickly set listeners on many views
    private List<SwitchButton> toggleButtons;
    private List<EditText> configEditTexts;
    private List<EditText> configEditTextsHex;

    private RibbonViewModel ribbonViewModel;

    /**
     * Onc Constructor to reule them all...
     *
     * @param activity A reference to the Activity that rcreateesa the sheet
     * @param configSheet ViewGroup of the sheet itself, from which individual views can be found
     * @param ribbonContainer ViewGroup that will contian the ProgressRibbon 8when not in orphan mode)
     */
    public ConfigSheetControl(AppCompatActivity activity,
                              ControlSheet configSheet,
                              ViewGroup ribbonContainer){

        DISPLAY_DENSITY=activity.getResources().getDisplayMetrics().density;
        DISPLAY_SCALED_DENSITY=activity.getResources().getDisplayMetrics().scaledDensity;
        this.mActivity=activity;
        this.controlSheet =configSheet;
        this.ribbonContainer=ribbonContainer;

        controlSheet.setNumberingMode(ControlSheet.NATURAL)
                .addControlSheetInflatedListener(this)
                .addControlStripButton(R.drawable.ic_reset_18dp, onResetButtonClick)
                .addControlStripButton(R.drawable.ic_play_18dp, onPlayButtonClick);

        //Need one to initialise ViewModel with. It will be set to all defaults fopr now
        progressRibbon=new ProgressRibbon(mActivity, ribbonContainer);
        initViewModel();
    }

    /**
     * The famed listener
     *
     * @param viewPager
     */
    @Override
    public void onControlSheetInflated(ViewPager viewPager) {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if(imm==null) {
                    imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                }
                imm.hideSoftInputFromWindow(viewPager.getWindowToken(), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });


        toggleButtons = new ArrayList<>();
        configEditTexts = new ArrayList<>();
        configEditTextsHex = new ArrayList<>();
        
        animationModeChooser = controlSheet.getViewPager().findViewById(R.id.animation_type_chooser);

        TextDrawable msDrawable = new TextDrawable(controlSheet.getViewPager().findViewById(R.id.animation_duration_value), " ms");
        TextDrawable hashDrawable = new TextDrawable(controlSheet.getViewPager().findViewById(R.id.ribbon_border_color_value), "#");
        TextDrawable spDrawable = new TextDrawable(controlSheet.getViewPager().findViewById(R.id.ribbon_text_size_value), " sp");
        dpDrawable = new TextDrawable(controlSheet.getViewPager().findViewById(R.id.ribbon_elevation_value), " dp");
        percentDrawable = new TextDrawable(controlSheet.getViewPager().findViewById(R.id.ribbon_margin_top_value), " %");

        for(Object view:controlSheet.getAllPagerItems()) {
            if (view instanceof SwitchButton) {
                ((SwitchButton) view).setCheckedChangeListener(onSwitchButtonCheckedChanged);
                toggleButtons.add((SwitchButton) view);
            } else if (view instanceof EditText){
                if(((EditText) view).getTag()!=null && ((EditText) view).getTag().equals("hextext")){
                    ((EditText) view).setCompoundDrawablesRelative(hashDrawable, null, null, null);
                    configEditTextsHex.add((EditText)view);
                } else {
                    //Hack-ish solution to allow for limited keyboard. the inputtypes are set to NumberPassword, and we need to show those numbers
                    ((EditText) view).setTransformationMethod(null);

                    String tag=null;

                    if(((EditText)view).getTag()!=null){
                        tag=((EditText)view).getTag().toString();
                    }

                    if(tag==null){
                        ((EditText)view).setCompoundDrawablesRelative(null, null, dpDrawable ,null);
                    } else if(tag.equals("textsize")){
                        ((EditText)view).setCompoundDrawablesRelative(null, null, spDrawable ,null);
                    } else if (tag.equals("time")){
                        ((EditText)view).setCompoundDrawablesRelative(null, null, msDrawable ,null);
                    }

                    configEditTexts.add((EditText) view);
                }
            }
        }

        initConfigViews();
        initRibbon();
    }

    /**
     * Config view get their beauty treatment here
     */
    private void initConfigViews(){

        //Set up the spinner for animation modes
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter.createFromResource(mActivity, R.array.animation_modes, android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        animationModeChooser.setAdapter(staticAdapter);

        //Editor action will onyl clear focuis since the focusChangeListener would handle their intended action(s) anyway
        TextView.OnEditorActionListener actionListener = (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                v.clearFocus();
            }
            return false;
        };

        View.OnFocusChangeListener focusChangeListener = (v, hasFocus) -> {
            if(!hasFocus){

                //If textviews are empty when focus is lost, reset them to their respective default values
                if(v.getTag()!=null && v.getTag().toString().equals("hextext")) {
                    if (((EditText) v).length() < 6) {
                        //setDefaultTextValues((EditText) v);
                        manageInputField((EditText) v, RESET);
                    }
                } else {
                    if (((EditText) v).length() < 1) {
                        //setDefaultTextValues((EditText) v);
                        manageInputField((EditText) v, RESET);
                    }
                }

                //Apply any value to theRibbon

                manageInputField((EditText)v, EVALUATE);
                //onTextFieldFocusLost((EditText)v);

                //If it's not really an EditText which has the focus, we no longer need the keyboard, do we?
                 if(!(mActivity.getCurrentFocus() instanceof EditText)) {
                     if (imm == null) {
                        imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    }
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        };


        // This InptutFiler will only allow valid #HEX colour values to be entered into colour related fields
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            CharSequence filteredSequence = source.toString().replaceAll("[^A-F^0-9]+", "");
            if(dest.length()>5){
                return "";
            }
            return filteredSequence;
        };

        for(EditText cET:configEditTexts){
      //      setDefaultTextValues(cET);
            manageInputField(cET, RESET);
            cET.setOnEditorActionListener(actionListener);
            cET.setOnFocusChangeListener(focusChangeListener);
        }

        for(EditText cETHex: configEditTextsHex){
            //setDefaultTextValues(cETHex);
            manageInputField(cETHex, RESET);
            cETHex.setOnEditorActionListener(actionListener);
            cETHex.setOnFocusChangeListener(focusChangeListener);
            cETHex.setFilters(new InputFilter[]{ filter });
        }

        animationModeChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 progressRibbon.setAnimationType(position);

                //Got to update dem LiveData
                 ribbonViewModel.postRibbonData(progressRibbon.getRibbonData());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }



    View.OnClickListener onPlayButtonClick= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           // Here we'll start the Ribon simulation
           if (controlSheet.getControlSheetState()!=BottomSheetBehavior.STATE_COLLAPSED) {
               controlSheet.collapseControlSheet();
            }

            //Hide the kb if showing
            if(imm==null) {
                imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            }
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            //Set some staring values ot the Ribbon
            progressRibbon.setProgress(0);

            if(progressRibbon.isIndeterminate()) {
                progressRibbon.setProgressText(R.string.infinite_progress);
            } else {
                progressRibbon.setProgressText(R.string.progress_ready);
            }

            //Disable some buttons under certain conditions
            if(progressRibbon.isInOrphanMode()){
                if(!progressRibbon.willBlockUnderlyingViews()) {
                    controlSheet.getViewPager().findViewById(R.id.block_view_toggle).setEnabled(false);
                    controlSheet.getViewPager().findViewById(R.id.orphan_mode_toggle).setEnabled(false);
                }
            } else {
                controlSheet.getViewPager().findViewById(R.id.orphan_mode_toggle).setEnabled(true);
            }

            //Show dem Ribbonm
            progressRibbon.show();

            // Start the simulation
            // AsyncTask to simulate the ProgressRibbon's progrss under various circumstances
            SimulateProgressAsync progressAsync = new SimulateProgressAsync(mActivity, progressRibbon, controlSheet, (MockupScrollerAdapter) ((RecyclerView)(ribbonContainer.findViewById(R.id.mockup_scroller))).getAdapter());
            progressAsync.execute(progressRibbon.isIndeterminate());

        }
    };

    View.OnClickListener onResetButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // Need to introduce a slight delay. We are doing a lot of work on the UI
            // thread when resetting buttons and views, and this can make the button animation choppy.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resetDefaults();
                        }
                });
            }}, ControlSheet.BUTTON_ANIMATION_DURATION) ;
        }
    };


    /**
     * OnClickListener for the sheet's various SwitchButtons
     * @param view The button in question
     */

    SwitchButton.OnCheckedChangeListener onSwitchButtonCheckedChanged = new SwitchButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(SwitchButton buttonView, boolean isChecked) {

            if (progressRibbon != null) {

                if (mActivity.getCurrentFocus() instanceof EditText) {
                    mActivity.getCurrentFocus().clearFocus();
                }

                switch (buttonView.getId()) {
                    case R.id.indeterminate_toggle: {
                        progressRibbon.setIndeterminateState(isChecked ? ProgressRibbon.INDETERMINATE : ProgressRibbon.DETERMINATE);

                        if (progressRibbon.isIndeterminate()) {
                            progressRibbon.setProgressText(R.string.infinite_progress);
                        }
                        break;
                    }
                    case R.id.result_as_percentage_toggle: {
                        progressRibbon.setReportProgressAsMaxPercent(isChecked);
                        break;
                    }
                    case R.id.orphan_mode_toggle: {
                        initRibbon();
                        break;
                    }
                    case R.id.dialogue_mode_toggle: {
                        progressRibbon.setRibbonInDialogueMode(isChecked);
                        break;
                    }
                    case R.id.block_view_toggle: {
                        progressRibbon.setViewBlocking(isChecked);
                        if (((SwitchButton)controlSheet.getViewPager().findViewById(R.id.orphan_mode_toggle)).isChecked()) {
                            RibbonControl.createRibbon(mActivity, progressRibbon, null, controlSheet.getViewPager());
                            System.gc();
                        }
                        break;
                    }
                    case R.id.text_beside_spinner_toggle: {
                        progressRibbon.setRibbonTextPosition(isChecked ? ProgressRibbon.TEXT_BESIDE_BAR : ProgressRibbon.TEXT_UNDER_BAR);
                        if (progressRibbon.isRibbonTextBesideProgressBar()) {
                            progressRibbon.setProgressBarStyle(ProgressRibbon.BAR_ROUND);
                            ((SwitchButton)controlSheet.getViewPager().findViewById(R.id.bar_indicator_toggle)).setChecked(false);
                            controlSheet.getViewPager().findViewById(R.id.bar_indicator_toggle).setEnabled(false);

                        } else {
                            progressRibbon.setRibbonTextPosition(ProgressRibbon.TEXT_UNDER_BAR);
                            controlSheet.getViewPager().findViewById(R.id.bar_indicator_toggle).setEnabled(true);
                        }
                        break;
                    }
                    case R.id.bar_indicator_toggle: {
                        if (isChecked) {
                            progressRibbon.setProgressBarStyle(ProgressRibbon.BAR_HORIZONTAL);
                            ((SwitchButton)controlSheet.getViewPager().findViewById(R.id.text_beside_spinner_toggle)).setChecked(false);
                            controlSheet.getViewPager().findViewById(R.id.text_beside_spinner_toggle).setEnabled(false);

                        } else {
                            controlSheet.getViewPager().findViewById(R.id.text_beside_spinner_toggle).setEnabled(true);
                            progressRibbon.setProgressBarStyle(ProgressRibbon.BAR_ROUND);
                        }
                        break;
                    }
                    case R.id.margin_as_percent_toggle: {

                        EditText marginTopValue=controlSheet.getViewPager().findViewById(R.id.ribbon_margin_top_value);
                        EditText marginBottomValue=controlSheet.getViewPager().findViewById(R.id.ribbon_margin_bottom_value);

                        if (isChecked) {
                            marginTopValue.setCompoundDrawablesRelative(null, null, percentDrawable, null);
                            marginBottomValue.setCompoundDrawablesRelative(null, null, percentDrawable, null);
                            progressRibbon.setRibbonMarginTop(ProgressRibbon.PARENT_HEIGHT_PERCENT, Integer.parseInt(marginTopValue.getText().toString()));
                            progressRibbon.setRibbonMarginBottom(ProgressRibbon.PARENT_HEIGHT_PERCENT, Integer.parseInt(marginBottomValue.getText().toString()));

                        } else {
                            marginTopValue.setCompoundDrawablesRelative(null, null, dpDrawable, null);
                            marginBottomValue.setCompoundDrawablesRelative(null, null, dpDrawable, null);
                            progressRibbon.setRibbonMarginTop(Integer.parseInt(marginTopValue.getText().toString()));
                            progressRibbon.setRibbonMarginBottom(Integer.parseInt(marginBottomValue.getText().toString()));
                        }
                        break;
                    }
                    case R.id.ribbon_transparent_toggle: {

                        EditText borderColorValue=controlSheet.getViewPager().findViewById(R.id.ribbon_border_color_value);
                        EditText backgorundColourValue=controlSheet.getViewPager().findViewById(R.id.ribbon_background_color_value);
                        EditText borderSizeValue=controlSheet.getViewPager().findViewById(R.id.ribbon_border_size_value);
                        EditText borderRadiusValue=controlSheet.getViewPager().findViewById(R.id.ribbon_border_radius_value);
                        EditText elevationValue=controlSheet.getViewPager().findViewById(R.id.ribbon_elevation_value);

                        float alpha = isChecked ? 0.5f : 1.0f;
                        borderColorValue.setEnabled(!isChecked);
                        borderColorValue.setAlpha(alpha);
                        backgorundColourValue.setEnabled(!isChecked);
                        backgorundColourValue.setAlpha(alpha);
                        borderSizeValue.setEnabled(!isChecked);
                        borderSizeValue.setAlpha(alpha);
                        borderRadiusValue.setEnabled(!isChecked);
                        borderRadiusValue.setAlpha(alpha);
                        elevationValue.setEnabled(!isChecked);
                        elevationValue.setAlpha(alpha);

                        if (isChecked) {
                            progressRibbon.setRibbonTransparent();
                        } else {
                            progressRibbon.setRibbonBackgroundColor(Color.parseColor("#" + backgorundColourValue.getText()));
                            progressRibbon.setRibbonBorderColor(Color.parseColor("#" + borderColorValue.getText()));
                        }
                        break;
                    }
                }

                ribbonViewModel.postRibbonData(progressRibbon.getRibbonData());
            }
        }
    };

    /**
     * Will either apply a default value to the supplied EditText, or apply its current value ot the Ribbon
     *
     * @param ed
     * @param action
     */
    private void manageInputField(EditText ed, int action){
        StringBuilder sb = new StringBuilder();

        if(action==EVALUATE){
            sb.append(ed.getText());
        } else {

        }

        switch (ed.getId()) {
            case R.id.ribbon_background_color_value: {
                if(action==RESET) {
                    sb.append(String.format("%06X", (0xFFFFFF & progressRibbon.getRibbonData().DEFAULT_RIBBON_BG_COLOR)));
                } else {
                    progressRibbon.setRibbonBackgroundColor(Color.parseColor("#" + sb.toString()));
                }
                break;
            }
            case R.id.ribbon_border_color_value: {
                if(action==RESET) {
                    sb.append(String.format("%06X", (0xFFFFFF & progressRibbon.getRibbonData().DEFAULT_RIBBON_BORDER_COLOR)));
                } else {
                    progressRibbon.setRibbonBorderColor(Color.parseColor("#" + sb.toString()));
                }
                break;
            }
            case R.id.ribbon_text_color_value: {
                if(action==RESET) {
                    sb.append(String.format("%06X", (0xFFFFFF & progressRibbon.getRibbonData().DEFAULT_RIBBON_TEXT_COLOR)));
                } else {
                    progressRibbon.setProgressTextColour(Color.parseColor("#"+sb.toString()));
                }
                break;
            }
            case R.id.ribbon_min_value:{
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_MIN);
                } else {
                    progressRibbon.setMin(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_max_value: {
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_MAX);
                } else {
                    progressRibbon.setMax(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.animation_duration_value: {
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_ANIMATION_DURATION);
                } else {
                    progressRibbon.setAnimationDuration(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_elevation_value: {
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_ELEVATION);
                } else {
                    progressRibbon.setElevation(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_border_size_value: {
                if(action==RESET) {
                    sb.append(Math.round(progressRibbon.getRibbonData().DEFAULT_RIBBON_BORDER_SIZE / DISPLAY_DENSITY));
                } else {
                    progressRibbon.setRibbonBorderSize(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_border_radius_value: {
                if(action==RESET) {
                    sb.append(Math.round(progressRibbon.getRibbonData().DEFAULT_RIBBON_BORDER_RADIUS / DISPLAY_DENSITY));
                } else {
                    progressRibbon.setRibbonBorderRadius(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_text_size_value: {
                if(action==RESET) {
                    sb.append(Math.round(progressRibbon.getRibbonData().DEFAULT_RIBBON_TEXT_SIZE / DISPLAY_SCALED_DENSITY));
                } else {
                    progressRibbon.setProgressTextSize(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_padding_top_value: {
                if (action == EVALUATE) {
                    progressRibbon.setRibbonPaddingTop(Integer.parseInt(sb.toString()));
                    break;
                }
            }
            case R.id.ribbon_padding_bottom_value: {
                if(action==RESET) {
                    sb.append(Math.round(progressRibbon.getRibbonData().DEFAULT_RIBBON_PADDING / DISPLAY_DENSITY));
                } else {
                    progressRibbon.setRibbonPaddingBottom(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_margin_top_value:
                if(action==EVALUATE){
                    if(progressRibbon.marginIsSetAsPercentage()) {
                        progressRibbon.setRibbonMarginTop(ProgressRibbon.PARENT_HEIGHT_PERCENT, Integer.parseInt(ed.getText().toString()));
                    } else {
                        progressRibbon.setRibbonMarginTop(Integer.parseInt(sb.toString()));
                    }
                    break;
                }
            case R.id.ribbon_margin_bottom_value:{
                if(action==RESET) {
                    if (progressRibbon.marginIsSetAsPercentage()) {
                        sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_MARGIN);
                    } else {
                        sb.append(Math.round(progressRibbon.getRibbonData().DEFAULT_RIBBON_MARGIN / DISPLAY_DENSITY));
                    }
                } else {
                    if(progressRibbon.marginIsSetAsPercentage()) {
                        progressRibbon.setRibbonMarginBottom(ProgressRibbon.PARENT_HEIGHT_PERCENT, Integer.parseInt(ed.getText().toString()));
                    } else {
                        progressRibbon.setRibbonMarginBottom(Integer.parseInt(sb.toString()));
                    }
                }
                break;
            }
            case R.id.ribbon_show_delay_value: {
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_SHOW_DELAY);
                } else{
                    progressRibbon.setShowDelay(Integer.parseInt(sb.toString()));
                }
                break;
            }
            case R.id.ribbon_hide_delay_value: {
                if(action==RESET) {
                    sb.append(progressRibbon.getRibbonData().DEFAULT_RIBBON_HIDE_DELAY);
                } else {
                    progressRibbon.setHideDelay(Integer.parseInt(sb.toString()));
                }
                break;
            }
            default:{
                if(action==RESET) {
                    sb.append("0");
                }
            }
        }

        if(action==RESET && sb.length()>0){
            ed.setText(sb);
        }

        // Got to update dem LiveData
        if(ribbonViewModel!=null) {
            ribbonViewModel.postRibbonData(progressRibbon.getRibbonData());
        }
    }

    /**
     * Resets everythign to default
     */
    private void resetDefaults(){

        for(SwitchButton button:toggleButtons){
            if(button.getId()==R.id.block_view_toggle || button.getId()==R.id.indeterminate_toggle){
                button.setChecked(true);
            } else {
                button.setChecked(false);
            }
        }

        animationModeChooser.setSelection(0);

        EditText marginTopValue=controlSheet.getViewPager().findViewById(R.id.ribbon_margin_top_value);
        EditText marginBottomValue=controlSheet.getViewPager().findViewById(R.id.ribbon_margin_top_value);

        marginTopValue.setCompoundDrawablesRelative(null, null, dpDrawable, null);
        marginBottomValue.setCompoundDrawablesRelative(null, null, dpDrawable, null);
        progressRibbon.setRibbonMarginTop(Integer.parseInt(marginTopValue.getText().toString()));
        progressRibbon.setRibbonMarginBottom(Integer.parseInt(marginBottomValue.getText().toString()));


        for(EditText cET:configEditTexts){
            //setDefaultTextValues(cET);
            manageInputField(cET, RESET);
        }
        for(EditText cETHex: configEditTextsHex){
            //setDefaultTextValues(cETHex);
            manageInputField(cETHex, RESET);
        }

        RibbonControl.resetRibbon(mActivity, progressRibbon, controlSheet.getViewPager());
        ribbonViewModel.postRibbonData(progressRibbon.getRibbonData());
    }

    /**
     * Initialises the progressRibbon to be used in the simulation
     */
    private void initRibbon() {

        if(controlSheet.getViewPager().findViewById(R.id.orphan_mode_toggle)!=null) {
            if (((SwitchButton)controlSheet.getViewPager().findViewById(R.id.orphan_mode_toggle)).isChecked()) {
                progressRibbon = RibbonControl.createRibbon(mActivity, progressRibbon, null, controlSheet.getViewPager());
            } else {
                progressRibbon = RibbonControl.createRibbon(mActivity, progressRibbon, ribbonContainer, controlSheet.getViewPager());
            }
            System.gc();
        } else {
            progressRibbon=new ProgressRibbon(mActivity, ribbonContainer);
        }
    }

    /**
     * Set up a viewmodeel, so that the Design and Code fragments can share its date and dynamically update themselves as needed
     * This is an unusual use-case pfor the prpogressRibbon, as it would only represent progress rather then being the main focus of
     * the application, but here the roles are reversed, sine this application is all about the Ribbon itself.
     * It's also a great way to show off some sweet VirewModel skillz, ha!
     */
    private void initViewModel(){

        if(ribbonViewModel==null) {
            ribbonViewModel = ViewModelProviders.of(mActivity).get(RibbonViewModel.class);
        }
        ribbonViewModel.setDisplayDensities(DISPLAY_DENSITY, DISPLAY_SCALED_DENSITY);
        ribbonViewModel.setRibbonData(progressRibbon.getRibbonData());
    }
}