/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.core;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import androidx.annotation.Nullable;

import com.atlanticomnibus.progressribbon.ProgressRibbon;

import java.lang.reflect.Field;


/**
 * This class holdssome easy-to use static methods for relatively painless code generation
 */
public class CodeGen {

    /****************************** Cmponents to build from **************************************/

    private static final String XML_OPENING_TAG = "<br />\u00A0\u00A0<span style=\"color:#660E7A\">app</span><span style=\"color:#0000FF\">:",
            XML_OPENING_TAG_ANDROID = "<br />\u00A0\u00A0<span style=\"color:#660E7A\">android</span><span style=\"color:#0000FF\">:",
            XML_MIDDLE_BIT = "=</span><span style=\"color:#008000\">\"",
            XML_CLOSING_TAG = "\"</span>";

    private static final String JAVA_INDENT = "<br />\u00A0\u00A0",
            JAVA_CONSTANT_START = "<strong><em><span style=\"color:#660E7A\">",
            JAVA_CONSTANT_END = "</span></em></strong>",
            JAVA_NUMBER_START = "<span style=\"color:#0000FF\">",
            JAVA_NUMBER_END = "</span>",
            JAVA_STRING_START = "<strong><span style=\"color:#008000\">\"",
            JAVA_STRING_END = "\"</span></strong>",
            JAVA_KEYWORD_START = "<strong><span style=\"color:#000080\">",
            JAVA_KEYWORD_END = "</span></strong>",
            JAVA_MEMBER_START = "<strong><span style=\"color:#660E7A\">",
            JAVA_VAR_START = "<span style=\"color:#660E7A\">",
            JAVA_VAR_END = "</span>",
            JAVA_MEMBER_END = "</span></strong>",
            JAVA_COMMENT_START = "<em><span style=\"color:#808080\">",
            JAVA_COMMENT_END = "</span></em><br />";

    private static final String SHARE_SEPARATOR = "\n\n------\n\n",
            SHARE_LINE_BREAK = "\n\n";


    /**
     * Will geneerate the XML code that can be used to recreate a  ProgressRibbon with the supplied data values,
     * complete with HTML tags for colour and styling reminiscent of AndroidStudio's representation
     *
     * @param ribbonData           RibbonData object to get values from. Since the ViewModel only stores a referecne to the RibbboinData Object
     * @param displayDensity       Taken from WindowManager class, this will be used to calculate DIP values on the fly
     * @param displayScaledDensity Taken from WindowManager class, this will be used to calculate SP values on the fly
     * @return a pretty HTML String with the generated XML code
     */
    public static String generateXMLCode(ProgressRibbon.RibbonData ribbonData, float displayDensity, float displayScaledDensity) {

        ProgressRibbon ribbon = getEnclosingRibbonInstance(ribbonData);

        if (ribbon != null) {

            StringBuilder sb = new StringBuilder();

            if (ribbon.isInOrphanMode()) {
                sb.append("XML declaration is not applicable to orphan ProgressRibbon");
            } else {
                sb.append("&#60;");
                sb.append("<span style=\"color:#000080\">com.atlanticomnibus.progressribbon.ProgressRibbon</span>");
                sb.append(XML_OPENING_TAG_ANDROID)
                        .append("layout_width")
                        .append(XML_MIDDLE_BIT)
                        .append("match_parent")
                        .append(XML_CLOSING_TAG)
                        .append(XML_OPENING_TAG_ANDROID)
                        .append("layout_height")
                        .append(XML_MIDDLE_BIT)
                        .append("match_parent")
                        .append(XML_CLOSING_TAG);

                if (ribbon.getProgressBarStyle() == ProgressRibbon.BAR_HORIZONTAL) {
                    sb.append(emitXMLLine("progressBarType", "horizontal", null));
                } else {
                    if (ribbon.isRibbonTextBesideProgressBar()) {
                        sb.append(emitXMLLine("progressBarType", "round", null));
                        sb.append(emitXMLLine("textBesideProgressBar", "true", null));
                    }
                }

                /**Behaviour**/
                if (!ribbon.isIndeterminate()) {
                    sb.append(emitXMLLine("isIndeterminate", "false", null));
                }

                if (ribbon.getShowDelay() != ribbonData.DEFAULT_RIBBON_SHOW_DELAY) {
                    sb.append(emitXMLLine("showDelay", ribbon.getShowDelay(), null));
                }

                if (ribbon.getHideDelay() != ribbonData.DEFAULT_RIBBON_HIDE_DELAY) {
                    sb.append(emitXMLLine("hideDelay", ribbon.getHideDelay(), null));
                }

                if (ribbon.getMin() != ribbonData.DEFAULT_RIBBON_MIN) {
                    sb.append(emitXMLLine("min", ribbon.getMin(), null));
                }

                if (ribbon.getMax() != ribbonData.DEFAULT_RIBBON_MAX) {
                    sb.append(emitXMLLine("max", ribbon.getMax(), null));
                }

                if (ribbon.willReportProgressAsMaxPercent()) {
                    sb.append(emitXMLLine("reportProgressAsMaxPercent", "true", null));
                }

                if (!ribbon.willBlockUnderlyingViews()) {
                    sb.append(emitXMLLine("blockUnderlyingViews", "false", null));
                }

                if (ribbon.getAnimationType() != ProgressRibbon.DO_NOT_ANIMATE) {

                    //Won't muck about with accessing Ribbon attributes, we have no other need for a ribbon instance, and it's simpler this way
                    String xmlAnimationType;
                    switch (ribbon.getAnimationType()) {
                        default:
                        case ProgressRibbon.ANIMATE_FADE: {
                            xmlAnimationType = "fade";
                            break;
                        }
                        case ProgressRibbon.ANIMATE_SCALE: {
                            xmlAnimationType = "scale";
                            break;
                        }
                        case ProgressRibbon.ANIMATE_SCALE_FADE: {
                            xmlAnimationType = "fade_scale";
                            break;
                        }
                    }

                    sb.append(emitXMLLine("animationType", xmlAnimationType, null));

                    if (ribbon.getAnimationDuration() != ribbonData.DEFAULT_ANIMATION_DURATION) {
                        sb.append(emitXMLLine("animationDuration", ribbon.getAnimationDuration(), null));
                    }
                }

                /**Appearance**/
                if (ribbon.getRibbonTopPadding() == ribbon.getRibbonBottomPadding()) {
                    if (ribbon.getRibbonTopPadding() != Math.round(ribbonData.DEFAULT_RIBBON_PADDING / displayDensity)) {
                        sb.append(emitXMLLine("ribbonPadding", ribbon.getRibbonTopPadding(), "dp"));
                    }
                } else {

                    if (ribbon.getRibbonTopPadding() != Math.round(ribbonData.DEFAULT_RIBBON_PADDING / displayDensity)) {
                        sb.append(emitXMLLine("ribbonPaddingTop", ribbon.getRibbonTopPadding(), "dp"));
                    }

                    if (ribbon.getRibbonBottomPadding() != Math.round(ribbonData.DEFAULT_RIBBON_PADDING / displayDensity)) {
                        sb.append(emitXMLLine("ribbonPaddingBottom", ribbon.getRibbonBottomPadding(), "dp"));
                    }
                }

                if (ribbon.marginIsSetAsPercentage()) {
                    if (ribbon.getRibbonMarginTop() != ribbonData.DEFAULT_RIBBON_MARGIN) {
                        sb.append(emitXMLLine("ribbonMarginPercentTop", ribbon.getRibbonMarginTop(), null));
                    }

                    if (ribbon.getRibbonMarginBottom() != ribbonData.DEFAULT_RIBBON_MARGIN) {
                        sb.append(emitXMLLine("ribbonMarginPercentBottom", ribbon.getRibbonMarginBottom(), null));
                    }
                } else {
                    if (ribbon.getRibbonMarginTop() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN / displayDensity)) {
                        sb.append(emitXMLLine("ribbonMarginTop", ribbon.getRibbonMarginTop(), "dp"));
                    }

                    if (ribbon.getRibbonMarginBottom() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN / displayDensity)) {
                        sb.append(emitXMLLine("ribbonMarginBottom", ribbon.getRibbonMarginBottom(), "dp"));
                    }
                }

                if (ribbon.isInDialogueMode()) {
                    sb.append(emitXMLLine("dialogueMode", "true", null));

                    if (!ribbon.isTransparent() && ribbon.getRibbonBorderRadius() != ribbonData.DEFAULT_RIBBON_BORDER_RADIUS) {
                        sb.append(emitXMLLine("borderRadius", Math.round(ribbon.getRibbonBorderRadius() / displayDensity), null));
                    }
                }


                if (!ribbon.isTransparent()) {
                    if (ribbon.getElevation() != ribbonData.DEFAULT_RIBBON_ELEVATION) {
                        sb.append(emitXMLLine("ribbonElevation", Math.round(ribbon.getElevation()), "dp"));
                    }

                    if (ribbon.getRibbonBorderSize() > 0) {

                        if (ribbon.getRibbonBorderSize() != Math.round(ribbonData.DEFAULT_RIBBON_BORDER_SIZE / displayDensity)) {
                            sb.append(emitXMLLine("borderThickness", ribbon.getRibbonBorderSize(), "dp"));
                        }

                        if (ribbon.getRibbonBorderSize() > 0) {
                            if (ribbon.getRibbonBorderColor() != ribbonData.DEFAULT_RIBBON_BORDER_COLOR) {
                                sb.append(emitXMLLine("borderColor", String.format("#%06X", (0xFFFFFF & ribbon.getRibbonBorderColor())), null));
                            }
                        }

                    } else {
                        sb.append(emitXMLLine("borderless", "true", null));
                    }

                    if (ribbon.getRibbonBackgroundColor() != ribbonData.DEFAULT_RIBBON_BG_COLOR) {
                        sb.append(emitXMLLine("ribbonBackgroundColor", String.format("#%06X", (0xFFFFFF & ribbon.getRibbonBackgroundColor())), null));
                    }

                    if (ribbon.getProgressTextColor() != ribbonData.DEFAULT_RIBBON_TEXT_COLOR) {
                        sb.append(emitXMLLine("progressTextColor", String.format("#%06X", (0xFFFFFF & ribbon.getProgressTextColor())), null));
                    }

                    if (ribbon.getProgressTextSize() != ribbonData.DEFAULT_RIBBON_TEXT_SIZE) {
                        sb.append(emitXMLLine("progressTextSize", Math.round(ribbon.getProgressTextSize() / displayScaledDensity), "sp"));
                    }
                } else {
                    sb.append(emitXMLLine("transparent", "true", null));
                }

                sb.append(" />");
            }

            return sb.toString();
        } else {
            return "There was an error. That is all you need to know at this pont.";
        }
    }


    /**
     * Will geneerate the Java code that can be used to recreate a  ProgressRibbon with the supplied data values,
     * complete with HTML tags for colour and styling reminiscent of AndroidStudio's representation
     *
     * @param ribbonData           RibbonData object to get values from. Since the ViewModel only stores a referecne to the RibbboinData Object
     * @param displayDensity       Taken from WindowManager class, this will be used to calculate DIP values on the fly
     * @param displayScaledDensity Taken from WindowManager class, this will be used to calculate SP values on the fly
     * @return a pretty HTML String with the generated Java code
     */
    public static String generateJavaCode(ProgressRibbon.RibbonData ribbonData, float displayDensity, float displayScaledDensity) {
        StringBuilder sb = new StringBuilder();

        ProgressRibbon ribbon = getEnclosingRibbonInstance(ribbonData);

        if (ribbon.isInOrphanMode()) {
            sb.append("ProgressRibbon ")
                    .append(JAVA_MEMBER_START)
                    .append("progressRibbon")
                    .append(JAVA_KEYWORD_END)
                    .append(" = ")
                    .append(JAVA_KEYWORD_START)
                    .append("new")
                    .append(JAVA_KEYWORD_END)
                    .append(" ProgressRibbon(")
                    .append(JAVA_VAR_START)
                    .append("mActivity")
                    .append(JAVA_VAR_END + ")");
        } else {
            sb.append("ProgressRibbon ")
                    .append(JAVA_MEMBER_START)
                    .append("progressRibbon")
                    .append(JAVA_KEYWORD_END)
                    .append(" = ")
                    .append(JAVA_KEYWORD_START)
                    .append("new")
                    .append(JAVA_KEYWORD_END)
                    .append(" ProgressRibbon(")
                    .append(JAVA_VAR_START)
                    .append("mContext" + JAVA_VAR_END)
                    .append(", " + JAVA_VAR_START)
                    .append("parentViewGroup")
                    .append(JAVA_VAR_END)
                    .append(")");
        }

        if (ribbon.getProgressBarStyle() == ProgressRibbon.BAR_HORIZONTAL) {
            sb.append(JAVA_INDENT)
                    .append(".setProgressBarStyle(ProgressRibbon.")
                    .append(JAVA_CONSTANT_START)
                    .append("BAR_HORIZONTAL")
                    .append(JAVA_CONSTANT_END)
                    .append(")");
        } else {
            if (ribbon.isRibbonTextBesideProgressBar()) {
                sb.append(JAVA_INDENT)
                        .append(".setProgressBarStyle(ProgressRibbon.")
                        .append(JAVA_CONSTANT_START)
                        .append("BAR_ROUND")
                        .append(JAVA_CONSTANT_END)
                        .append(")")
                        .append(JAVA_INDENT)
                        .append(".setRibbonTextPosition(ProgressRibbon.")
                        .append(JAVA_CONSTANT_START)
                        .append("TEXT_BESIDE_BAR")
                        .append(JAVA_CONSTANT_END)
                        .append(")");
            }
        }

        /**Behaviour**/
        if (!ribbon.isIndeterminate()) {
            sb.append(JAVA_INDENT)
                    .append(".setIndeterminateState(ProgressRibbon.")
                    .append(JAVA_CONSTANT_START)
                    .append("DETERMINATE")
                    .append(JAVA_CONSTANT_END)
                    .append(")");
        }

        if (ribbon.getShowDelay() != ribbonData.DEFAULT_RIBBON_SHOW_DELAY) {
            sb.append(JAVA_INDENT)
                    .append(".setShowDelay(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getShowDelay())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.getHideDelay() != ribbonData.DEFAULT_RIBBON_HIDE_DELAY) {
            sb.append(JAVA_INDENT)
                    .append(".setHideDelay(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getHideDelay())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.getMin() != ribbonData.DEFAULT_RIBBON_MIN) {
            sb.append(JAVA_INDENT)
                    .append(".setMin(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getMin())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.getMax() != ribbonData.DEFAULT_RIBBON_MAX) {
            sb.append(JAVA_INDENT)
                    .append(".setMax(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getMax())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.willReportProgressAsMaxPercent()) {
            sb.append(JAVA_INDENT)
                    .append(".setReportProgressAsMaxPercent(")
                    .append(JAVA_KEYWORD_START)
                    .append("true")
                    .append(JAVA_KEYWORD_END)
                    .append(")");
        }

        if (!ribbon.willBlockUnderlyingViews()) {
            sb.append(JAVA_INDENT)
                    .append(".setViewBlocking(")
                    .append(JAVA_KEYWORD_START)
                    .append("false")
                    .append(JAVA_KEYWORD_END)
                    .append(")");
        }

        if (ribbon.getAnimationType() != ProgressRibbon.DO_NOT_ANIMATE) {
            StringBuilder animationTypeConstant = new StringBuilder();

            switch (ribbon.getAnimationType()) {
                default:
                case ProgressRibbon.ANIMATE_FADE: {
                    animationTypeConstant.append("ProgressRibbon.")
                            .append(JAVA_CONSTANT_START)
                            .append("ANIMATE_FADE")
                            .append(JAVA_CONSTANT_END);
                    break;
                }
                case ProgressRibbon.ANIMATE_SCALE: {
                    animationTypeConstant.append("ProgressRibbon.")
                            .append(JAVA_CONSTANT_START)
                            .append("ANIMATE_SCALE")
                            .append(JAVA_CONSTANT_END);
                    break;
                }
                case ProgressRibbon.ANIMATE_SCALE_FADE: {
                    animationTypeConstant.append("ProgressRibbon.")
                            .append(JAVA_CONSTANT_START)
                            .append("ANIMATE_SCALE_FADE")
                            .append(JAVA_CONSTANT_END);
                    break;
                }
            }

            sb.append(JAVA_INDENT)
                    .append(".setAnimationType(")
                    .append(animationTypeConstant.toString())
                    .append(")");

            if (ribbon.getAnimationDuration() != ribbonData.DEFAULT_ANIMATION_DURATION) {
                sb.append(JAVA_INDENT)
                        .append(".setAnimationDuration(")
                        .append(JAVA_NUMBER_START)
                        .append(ribbon.getAnimationDuration())
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }
        }


        /**Appearance**/
        if (ribbon.getRibbonTopPadding() != Math.round(ribbonData.DEFAULT_RIBBON_PADDING / displayDensity)) {
            sb.append(JAVA_INDENT)
                    .append(".setRibbonPaddingTop(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getRibbonTopPadding())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.getRibbonBottomPadding() != Math.round(ribbonData.DEFAULT_RIBBON_PADDING / displayDensity)) {
            sb.append(JAVA_INDENT)
                    .append(".setRibbonPaddingBottom(")
                    .append(JAVA_NUMBER_START)
                    .append(ribbon.getRibbonBottomPadding())
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        if (ribbon.marginIsSetAsPercentage()) {
            if (ribbon.getRibbonMarginTop() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN)) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonMarginTop(ProgressRibbon.")
                        .append(JAVA_CONSTANT_START)
                        .append("PARENT_HEIGHT_PERCENT")
                        .append(JAVA_CONSTANT_END)
                        .append(", ")
                        .append(JAVA_NUMBER_START)
                        .append(ribbon.getRibbonMarginTop())
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }

            if (ribbon.getRibbonMarginBottom() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN)) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonMarginBottom(ProgressRibbon.")
                        .append(JAVA_CONSTANT_START)
                        .append("PARENT_HEIGHT_PERCENT")
                        .append(JAVA_CONSTANT_END)
                        .append(", ")
                        .append(JAVA_NUMBER_START)
                        .append(ribbon.getRibbonMarginBottom())
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }
        } else {
            if (ribbon.getRibbonMarginTop() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN / displayDensity)) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonMarginTop(TypedValue.")
                        .append(JAVA_CONSTANT_START)
                        .append("COMPLEX_UNIT_DIP")
                        .append(JAVA_CONSTANT_END)
                        .append(", ")
                        .append(JAVA_NUMBER_START)
                        .append(ribbon.getRibbonMarginTop())
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }

            if (ribbon.getRibbonMarginBottom() != Math.round(ribbonData.DEFAULT_RIBBON_MARGIN / displayDensity)) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonMarginBottom(TypedValue.")
                        .append(JAVA_CONSTANT_START)
                        .append("COMPLEX_UNIT_DIP")
                        .append(JAVA_CONSTANT_END)
                        .append(", ")
                        .append(JAVA_NUMBER_START)
                        .append(ribbon.getRibbonMarginBottom())
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }
        }

        if (ribbon.isInDialogueMode()) {
            sb.append(JAVA_INDENT)
                    .append(".setRibbonInDialogueMode(")
                    .append(JAVA_KEYWORD_START)
                    .append("true")
                    .append(JAVA_KEYWORD_END)
                    .append(")");

            if (ribbon.getRibbonBorderRadius() != ribbonData.DEFAULT_RIBBON_BORDER_RADIUS) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonBorderRadius(")
                        .append(JAVA_NUMBER_START)
                        .append(Math.round(ribbon.getRibbonBorderRadius() / displayDensity))
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }
        }

        if (!ribbon.isTransparent()) {

            if (ribbon.getRibbonBorderSize() > 0) {
                if (ribbon.getRibbonBorderSize() != Math.round(ribbonData.DEFAULT_RIBBON_BORDER_SIZE / displayDensity)) {
                    sb.append(JAVA_INDENT)
                            .append(".setRibbonBorderSize(")
                            .append(JAVA_NUMBER_START)
                            .append(ribbon.getRibbonBorderSize())
                            .append(JAVA_NUMBER_END)
                            .append(")");
                }

                if (ribbon.getRibbonBorderColor() != ribbonData.DEFAULT_RIBBON_BORDER_COLOR) {
                    sb.append(JAVA_INDENT)
                            .append(".setRibbonBorderColor(")
                            .append(JAVA_STRING_START)
                            .append(String.format("#%06X", (0xFFFFFF & ribbon.getRibbonBorderColor())))
                            .append(JAVA_STRING_END)
                            .append(")");
                }
            } else {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonBorderless()");

            }

            if (ribbon.getRibbonBackgroundColor() != ribbonData.DEFAULT_RIBBON_BG_COLOR) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonBackgroundColor(")
                        .append(JAVA_STRING_START)
                        .append(String.format("#%06X", (0xFFFFFF & ribbon.getRibbonBackgroundColor())))
                        .append(JAVA_STRING_END)
                        .append(")");
            }

            if (ribbon.getElevation() != ribbonData.DEFAULT_RIBBON_ELEVATION) {
                sb.append(JAVA_INDENT)
                        .append(".setRibbonElevation(")
                        .append(JAVA_NUMBER_START)
                        .append(Math.round(ribbon.getElevation()))
                        .append(JAVA_NUMBER_END)
                        .append(")");
            }
        } else {
            sb.append(JAVA_INDENT)
                    .append(".setRibbonTransparent()");
        }

        if (ribbon.getProgressTextColor() != ribbonData.DEFAULT_RIBBON_TEXT_COLOR) {
            sb.append(JAVA_INDENT)
                    .append(".setProgressTextColour(")
                    .append(JAVA_STRING_START)
                    .append(String.format("#%06X", (0xFFFFFF & ribbon.getProgressTextColor())))
                    .append(JAVA_STRING_END)
                    .append(")");
        }

        if (ribbon.getProgressTextSize() != ribbonData.DEFAULT_RIBBON_TEXT_SIZE) {
            sb.append(JAVA_INDENT)
                    .append(".setProgressTextSize(")
                    .append(JAVA_NUMBER_START)
                    .append(Math.round(ribbon.getProgressTextSize() / displayScaledDensity))
                    .append(JAVA_NUMBER_END)
                    .append(")");
        }

        sb.append(";");


        return sb.toString();
    }

    /**
     * This will genreate the usage code (Java) with pretty HTML colurs and styling
     *
     * @param ribbonData Same as above
     * @return what it says
     */
    public static String generateAdditionalCode(ProgressRibbon.RibbonData ribbonData) {

        ProgressRibbon ribbon = getEnclosingRibbonInstance(ribbonData);

        StringBuilder sb = new StringBuilder();
        sb.append("<p>")
                .append(JAVA_COMMENT_START)
                .append("/*Whenever you need to show your Ribbon*/")
                .append(JAVA_COMMENT_END)
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".show();</p>");

        if (ribbon.getShowDelay() == ribbonData.DEFAULT_RIBBON_SHOW_DELAY) {
            sb.append("<p>")
                    .append(JAVA_COMMENT_START)
                    .append("/*To show your Ribbon wth ad-hoc `showDelay` (also overrides any preset value, use any integer value):*")
                    .append(JAVA_COMMENT_END)
                    .append(JAVA_MEMBER_START)
                    .append("progressRibbon")
                    .append(JAVA_MEMBER_END)
                    .append(".show(")
                    .append(JAVA_NUMBER_START)
                    .append("300")
                    .append(JAVA_NUMBER_END)
                    .append(");</p>");
        }

        sb.append("<p>")
                .append(JAVA_COMMENT_START)
                .append("/*To adjust progress value (use any integer value)*/")
                .append(JAVA_COMMENT_END)
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".setProgress(")
                .append(JAVA_NUMBER_START)
                .append("1")
                .append(JAVA_NUMBER_END)
                .append(");<br />")
                .append(JAVA_COMMENT_START + "/*or*/</span></em><br />")
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".incrementProgressBy(")
                .append(JAVA_NUMBER_START)
                .append("1")
                .append(JAVA_NUMBER_END)
                .append(");</p>");

        sb.append("<p>")
                .append(JAVA_COMMENT_START)
                .append("/*To change progress text*/")
                .append(JAVA_COMMENT_END)
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".setProgressText(")
                .append(JAVA_STRING_START)
                .append("Progress text")
                .append(JAVA_STRING_END)
                .append(");</p>");

        sb.append("<p>")
                .append(JAVA_COMMENT_START)
                .append("/*To hide Ribbon*/")
                .append(JAVA_COMMENT_END)
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".hide();</p>");

        sb.append("<p>")
                .append(JAVA_COMMENT_START)
                .append("/*To completely remove dynamically created Ribbon*/")
                .append(JAVA_COMMENT_END)
                .append(JAVA_MEMBER_START)
                .append("progressRibbon")
                .append(JAVA_MEMBER_END)
                .append(".removeDynamicRibbon();</p>");

        return sb.toString();
    }

    /**
     * Builds a String from stripped code-strings sans the HTML). imstead of using the above bulders, it will make used of the Strings supplied ot it
     * (taken from textView elsewhere)
     *
     * @param xmlCode   Stripped XML code
     * @param javaCode  Stripped Java code
     * @param usageCode Stripoped usage (Java) code
     * @return String of all the above strung together in pone String that can be shared
     */
    public static String prepareCodeForSharing(CharSequence xmlCode, CharSequence javaCode, CharSequence usageCode) {

        StringBuilder sb = new StringBuilder();

        sb.append("/*XML:*/")
                .append(SHARE_LINE_BREAK)
                .append(xmlCode)
                .append(SHARE_SEPARATOR)
                .append("/*Java*/:")
                .append(SHARE_LINE_BREAK)
                .append(javaCode)
                .append(SHARE_SEPARATOR)
                .append("/*Usage*/:")
                .append(SHARE_LINE_BREAK)
                .append(usageCode);

        return sb.toString();
    }

    /**
     * Convert basic HTML input into Spanned to be used in TextViews in version agnostic way
     *
     * @param html HTML text to be converted
     * @return Spanned object with the correct formatting
     */
    public static Spanned convertHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }


    /**
     * This will emit a single line for the XML code, applying the HTML styling, and making use of the supplied parameters
     * Since XMl is a quite repetative, this makes sense there. Java would be trickier, it's probably not really worth the effort
     *
     * @param keyword XML keyword
     * @param value   XML value
     * @param unit    optional unit (can be null)
     * @return Formatted XML line
     */
    private static String emitXMLLine(String keyword, Object value, @Nullable String unit) {

        StringBuilder sb = new StringBuilder();

        sb.append(XML_OPENING_TAG)
                .append(keyword)
                .append(XML_MIDDLE_BIT)
                .append(value);

        if (unit != null) {
            sb.append(unit);
        }

        sb.append(XML_CLOSING_TAG);

        return sb.toString();

    }

    /**
     * Ugly or not, this is a useful hack to simplify things. usng reflectionn we get hold of the ProgressRibbon that encloses RibbonData.
     * Since we only have access to the data itself,and most of its members are private, this wy we can avoid using reflection everywhere.
     * (Reference to ProgressRibbon itself <em>could</em> be kept the ViewModel, but the VM only needs the data itself, and we are assuming it's dumb enough
     * to know noting of its later usage)
     *
     * @param ribbonData ProgressRibbon.RibbonData objecrt to coerce its oter class frm
     * @return the ProgressRibbon instance enclosing our porecious RibbonData
     */
    private static ProgressRibbon getEnclosingRibbonInstance(ProgressRibbon.RibbonData ribbonData) {

        Field field;
        try {
            field = ProgressRibbon.RibbonData.class.getDeclaredField("this$0");
            field.setAccessible(true);

            // Dereference and cast it
            return (ProgressRibbon) field.get(ribbonData);

        } catch (NoSuchFieldException e) {
            Log.e("Reflection exception", "You should probably reflect on how you've even done that. Exceptional skill at breaking stuff.");
            return null;
        } catch (IllegalAccessException e) {
            Log.e("Reflection exception", "You should probably reflect on how you've even done that. Exceptional skill at breaking stuff.");
            return null;
        }
    }

    /**
     * Made this to help reflecting on the futility on existence. And also on some additional fields that might not have public setters, would the need arise ny time later
     *
     * @param ribbonData   the RibbonData instance to reflect from
     * @param declaredName anme of the field to be reflected
     * @return the Object reflected thusly
     */

    private static Object accessPrivateField(ProgressRibbon.RibbonData ribbonData, String declaredName) {

        try {
            Field f = ProgressRibbon.RibbonData.class.getDeclaredField(declaredName);
            f.setAccessible(true);
            return f.get(ribbonData);
        } catch (Exception e) {
            Log.e("Reflection exception", "You should probably reflect on how you've even done that. Exceptional skill at breaking stuff.");
            return null;
        }

    }
}
