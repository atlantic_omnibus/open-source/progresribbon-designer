/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.ribbon;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.atlanticomnibus.progressribbon.ProgressRibbon;
import com.atlanticomnibus.ribbondesigner.core.CodeGen;

/**
 * A ViewModel to hold all the Ribbon's data so it can be shared between fragments (Design and Coide)
 */
public class RibbonViewModel extends ViewModel {

    /**
     * What better place to store these, eh? unlikely to change, but how knows (thence, not final)
     */
    private float displayDensity;
    private float displayScaledDensity;

    /**
     * It1s mutable, it's lieve, and it's data
     */
    private MutableLiveData<ProgressRibbon.RibbonData> mRibbonData = new MutableLiveData<>();

    /**
     * The LiveData classes representing the code generated from the RibbonData, as/when it changes
     */
    private LiveData<String> codeTextXml = Transformations.map(mRibbonData, new Function<ProgressRibbon.RibbonData, String>() {
        @Override
        public String apply(ProgressRibbon.RibbonData input) {
            return CodeGen.generateXMLCode(input, displayDensity, displayScaledDensity);
        }
    });

    private LiveData<String> codeTextJava = Transformations.map(mRibbonData, new Function<ProgressRibbon.RibbonData, String>() {
        @Override
        public String apply(ProgressRibbon.RibbonData input) {
            return CodeGen.generateJavaCode(input, displayDensity, displayScaledDensity);
        }
    });

    private LiveData<String> codeTextUsage = Transformations.map(mRibbonData, CodeGen::generateAdditionalCode);

    /**
     * methods to set and change RibbonData
     */

    public void setRibbonData(ProgressRibbon.RibbonData data) {
        mRibbonData.setValue(data);
    }

    public void postRibbonData(ProgressRibbon.RibbonData data) {
        mRibbonData.postValue(data);
    }

    public LiveData<String> getXmlCodeText() {
        return codeTextXml;
    }

    public LiveData<String> getJavaCodeText() {
        return codeTextJava;
    }

    public LiveData<String> getusageCodeText() {
        return codeTextUsage;
    }

    /**
     * Currently unused method to return the raw RibbnData
     * @return the raw RibbonData
     */
    public LiveData<ProgressRibbon.RibbonData> getRawRibbonData() { return mRibbonData; }

    /**
     * Simple way to store approproiate diplay densities
     * @param density that
     * @param scaledDensity this' for SP
     */
    public void setDisplayDensities(float density, float scaledDensity) {
        this.displayDensity=density;
        this.displayScaledDensity=scaledDensity;
    }
}