/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.main;

import android.app.Activity;
import android.view.ViewGroup;

import java.io.Serializable;

/**
 * This solution is based on <https://github.com/stephanenicolas/activtity-fragment-lambda/blob/master/app/src/main/java/com/example/fragmentactivitylambda/HeadlineListener.java>
 *
 * The listener interface which is independent of the activity.
 * It must have a generic type to trigger target type inference.
 * It is serializable to ensure the listener can survive rotations.
 */
interface FragmentInitialisedListener<T extends Activity> extends Serializable {
    /**
     * Single method to get a functional interface. It uses the activity type
     * {@code T} to trigger type inference.
     * @param activity any activity instance.
     * @para ribbon the second param, which is the ProgressRibbon that will be assed down thorugh this interface.
     */
    void onFragmentInitialised(T activity, ViewGroup ribbonContainer);

}