/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.atlanticomnibus.ribbondesigner.R;
import com.atlanticomnibus.ribbondesigner.core.CodeGen;
import com.atlanticomnibus.ribbondesigner.ui.main.ViewAnimations;

import net.cachapa.expandablelayout.ExpandableLayout;

public class AboutActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        /**
         * These two get special treamment from HTML text, MovementMethod-s are for to make links clickable
         */
        ((TextView)findViewById(R.id.license_info_textview)).setText(CodeGen.convertHtml(getString(R.string.about_licenses_html)));
        ((TextView)findViewById(R.id.license_info_textview)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView)findViewById(R.id.libs_info_textview)).setText(CodeGen.convertHtml(getString(R.string.about_libraries_html)));
        ((TextView)findViewById(R.id.libs_info_textview)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * Since we have an activity from which these are accesisble, we can do the onClick the simple way
     *
     * This onClick method is applied to all expandable headers so they know whatto do ith themselves
     *
     * @param v the View being clicked
     */
    public void onAboutHeaderClick(View v){
        ExpandableLayout aboutExpandableLayout=null;
        //Yeah, it'll get animated 'n all
        View chevron=null;

        //Find the ExpandableLayout and chevron ImageView belonging to this particular header
        switch (v.getId()){
            case R.id.publisher_info_header:{
                aboutExpandableLayout = ((ViewGroup)v.getParent()).findViewById(R.id.pub_info_expandable_container);
                chevron = ((ViewGroup)v.getParent()).findViewById(R.id.pub_info_chevron);
                break;
            }
            case R.id.developer_info_header:{
                aboutExpandableLayout = ((ViewGroup)v.getParent()).findViewById(R.id.dev_info_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.dev_info_chevron);
                break;
            }
            case R.id.libraries_info_header:{
                aboutExpandableLayout = ((ViewGroup)v.getParent()).findViewById(R.id.libs_info_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.libs_info_chevron);
                break;
            }
            case R.id.licenses_info_header:{
                aboutExpandableLayout = ((ViewGroup)v.getParent()).findViewById(R.id.license_info_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.license_info_chevron);
                break;
            }
        }

        if(aboutExpandableLayout!=null && chevron!=null) {
            //Open/close the expandable layout below
            aboutExpandableLayout.toggle();
            //Pretty way to turn the chevron up or down
            ViewAnimations.animateCodeHeaderChevron(chevron, aboutExpandableLayout.isExpanded());
        }
    }

    /**
     * Whatever happenes when one clicks a social icon? They get social links...
     * @param v
     */
    public void onSocialIconClick(View v){

        String uri;
        Intent intent;

        //Links are stored in String tags for convenience
        if(v.getTag()!=null){
            uri=v.getTag().toString();

            //Either email address or web address
            if(uri.startsWith("mailto:")){
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("*/*");
                intent.setData(Uri.parse(uri)); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {uri.substring(6)});
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject_line));
            } else {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(uri));
            }

            //Look, daddy! They spin!
            ViewAnimations.animateButtonSpin(v);

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, getString(R.string.no_app_exists), Toast.LENGTH_SHORT).show();
            }

        } else {
            ViewAnimations.shakeButtonToSayNo(v);
        }
    }
}
