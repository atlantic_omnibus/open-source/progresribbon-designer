/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

import java.util.concurrent.TimeUnit;


/**
 * Static, convenietn animations for Views
 */
public class ViewAnimations {

    /**
     * Simple animation to spin a view 360degrees around its on midpoint. Used (here) to animate buttons
     * @param view
     */
    public static void animateButtonSpin(final View view){
        view.setPivotY((float)view.getMeasuredHeight()/2.0f);
        view.setPivotX((float)view.getMeasuredWidth()/2.0f);
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "rotation", 0, 360);
        animator.setDuration(300);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.start();
    }

    /**
     * View will "shake itself" to "say no"
     * @param view View to animate
     */
    public static void shakeButtonToSayNo(final View view){

        float translation=(float)view.getMeasuredWidth()/5.0f;

        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "TranslationX", 0-translation, translation);
        animator.setDuration(65);
        animator.setRepeatMode(ObjectAnimator.REVERSE);
        animator.setRepeatCount(5);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setTranslationX(0);
            }
        });

        animator.start();
    }

    /**
     * Simple animation to urn chevron up and down
     * @param chevron The chevron to turn up and dow
     * @param isOpen Whether it's open or not (decides which way to turn)
     */
    public static void animateCodeHeaderChevron(final View chevron, boolean isOpen){

        int from, to;

        if(isOpen){
            from=0;
            to=180;
        } else{
            from=180;
            to=0;
        }

        ObjectAnimator animator = ObjectAnimator.ofFloat(chevron, "rotation", from, to);
        animator.setDuration(300);
        animator.start();
    }
}
