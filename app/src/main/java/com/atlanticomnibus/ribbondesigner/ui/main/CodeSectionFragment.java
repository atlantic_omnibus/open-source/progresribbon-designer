/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.atlanticomnibus.ribbondesigner.R;
import com.atlanticomnibus.ribbondesigner.core.CodeGen;
import com.atlanticomnibus.ribbondesigner.ui.ribbon.RibbonViewModel;

import net.cachapa.expandablelayout.ExpandableLayout;


/**
 * Fragment to hosue the generated codes, (second page in the ViewPager)
 */
public class CodeSectionFragment extends Fragment {

    //ViewModel that golds RibbonData as LiveData.
    private RibbonViewModel ribbonViewModel;

    //The three Musketeers
    private TextView xmlCodeTextView;
    private TextView javaCodeTextView;
    private TextView usageCodeTextView;

    public static CodeSectionFragment newInstance(){
        return new CodeSectionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //get that viewmodel, yeah
        ribbonViewModel = ViewModelProviders.of(getActivity()).get(RibbonViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_code, container, false);

        xmlCodeTextView = rootView.findViewById(R.id.xml_code_textview);
        javaCodeTextView = rootView.findViewById(R.id.java_code_textview);
        usageCodeTextView = rootView.findViewById(R.id.usage_code_textview);

        rootView.findViewById(R.id.xml_code_header).setOnClickListener(onCodeHeaderClick);
        rootView.findViewById(R.id.java_code_header).setOnClickListener(onCodeHeaderClick);
        rootView.findViewById(R.id.usage_code_header).setOnClickListener(onCodeHeaderClick);

        /**
         * Observe any changes in the ViewModel, and generate the appropriate code HTML text with static functions
         * Like a boss.
         */
        ribbonViewModel.getXmlCodeText().observe(getActivity(), s -> xmlCodeTextView.setText(CodeGen.convertHtml(s)));
        ribbonViewModel.getJavaCodeText().observe(getActivity(), s -> javaCodeTextView.setText(CodeGen.convertHtml(s)));
        ribbonViewModel.getusageCodeText().observe(getActivity(), s -> usageCodeTextView.setText(CodeGen.convertHtml(s)));

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_code_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.share) {
            shareCodetext();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Clicking headers will expand/collapse the views below them, and also animate the chevrons
     */
    private final View.OnClickListener onCodeHeaderClick = v -> {
        ExpandableLayout codeHolder=null;
        View chevron=null;

        //Find the correct views to manuipulate
        switch(v.getId()){
            case R.id.xml_code_header:{
                codeHolder = ((ViewGroup)v.getParent()).findViewById(R.id.xml_code_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.xml_code_chevron);
                break;
            }
            case R.id.java_code_header:{
                codeHolder = ((ViewGroup)v.getParent()).findViewById(R.id.java_code_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.java_code_chevron);
                break;
            }
            case R.id.usage_code_header:{
                codeHolder = ((ViewGroup)v.getParent()).findViewById(R.id.usage_code_expandable_container);
                chevron=((ViewGroup)v.getParent()).findViewById(R.id.usage_code_chevron);
                break;
            }
        }

        if(codeHolder!= null && chevron!=null) {
            //This will expand/close the collapsible view
            codeHolder.toggle();
            //Pretty way to turn chevron
            ViewAnimations.animateCodeHeaderChevron(chevron, codeHolder.isExpanded());
        }
    };

    //Does what is says
    private void shareCodetext(){

        //Static function will prepare all code texts for sharing. We used the Raw tet from teh textviews, since we no longer need the HTML formatting
        String sharedCode=CodeGen.prepareCodeForSharing(xmlCodeTextView.getText(), javaCodeTextView.getText(), usageCodeTextView.getText());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sharedCode);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }
}
