/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.ui.main;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.atlanticomnibus.ribbondesigner.R;


/**
 * This Fragment holds the placeholder Views and the ViewHolder that acts as a container for the
 * attached version of the ProgressRibbon
 */
class DesignSectionFragment extends Fragment {

    //Fragment initialised listener as a static String constant. (Yeah, I know...)
    private static final String FRAGMENT_INITIALISED_LISTENER = "FRAGMENT_INITIALISED_LISTENER";

    //What does it look like? Yeah, it's exactly that
    private ViewGroup ribbonContainer;
    private RecyclerView mockUpScroller;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_design, container, false);
        setHasOptionsMenu(true);
        ribbonContainer=rootView.findViewById(R.id.ribbon_container);
        mockUpScroller=rootView.findViewById(R.id.mockup_scroller);

        MockupScrollerAdapter adapter;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            adapter = new MockupScrollerAdapter(getContext().getDrawable(R.drawable.mockup1));
        } else {
            adapter = new MockupScrollerAdapter(getContext().getResources().getDrawable(R.drawable.mockup1));
        }

        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        mockUpScroller.setLayoutManager(lm);
        mockUpScroller.setAdapter(adapter);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /**
         * Here we use the listener set from MainActivity (if we have it) to pass down the pőroperly inflated ribbonContainer
         */
        if (getArguments().containsKey(FRAGMENT_INITIALISED_LISTENER)) {
            final FragmentInitialisedListener fragmentInitialisedListener =
                    (FragmentInitialisedListener) getArguments().getSerializable(FRAGMENT_INITIALISED_LISTENER);

            //Pass down the View that the Ribbon will be attached ot, back to the MainActivity, so it can be used.
            fragmentInitialisedListener.onFragmentInitialised(getActivity(),  ribbonContainer);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_design_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * the single menu item constrols the visibility of the placeholder mockups
         */
        if(item.getItemId()==R.id.show_ipsum){
            if(item.isCheckable()) {
                if (item.isChecked()) {
                    item.setChecked(false);
                    mockUpScroller.setVisibility(View.GONE);
                } else {
                    item.setChecked(true);
                    mockUpScroller.setVisibility(View.VISIBLE);
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * The Builder design and the use of the interface are inspired by the article:
     * <https://medium.com/groupon-eng/from-fragments-to-activity-the-lambda-way-32c768c72aa9>
     *
     * The method itself is taken from https://github.com/stephanenicolas/activtity-fragment-lambda/blob/master/app/src/main/java/com/example/fragmentactivitylambda/MainFragment.java,
     * which itself seems to includes the full text of the Apache 2.0 license, but no copyright notice anywhere, which means the license, unfortunately, is not  applied ot the code.
     * Next time you want to read the appendix, sorry.
     *
     * Builds a {@link DesignSectionFragment} and put its communication lambda wit the activity
     * in the arguments so that they survive rotation.
     */
    static class Builder {
        private FragmentInitialisedListener fragmentInitialisedListener;

        /**
         * This method must define a type generic so that it triggers target type inference.
         * @param initListener the listener of clicks on an article.
         * @param <T> the type of the activity that will hold the method reference listener.
         * @return the builder itself for method chaining.
         */
        <T extends Activity> Builder setInitListener(FragmentInitialisedListener<T> initListener) {
            fragmentInitialisedListener = initListener;
            return this;
        }

        DesignSectionFragment build() {
            DesignSectionFragment mainFragment = new DesignSectionFragment();
            mainFragment.setArguments(createArgs());
            return mainFragment;
        }

        private Bundle createArgs() {
            Bundle bundle = new Bundle();

            if (fragmentInitialisedListener != null) {
                //store the listener in the arguments bundle
                //it is a state less lambda, guaranteed to be serializable

                bundle.putSerializable(FRAGMENT_INITIALISED_LISTENER, fragmentInitialisedListener);
            }
            return bundle;
        }
    }
}
