package com.atlanticomnibus.ribbondesigner.ui.main;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.atlanticomnibus.ribbondesigner.R;

public class MockupScrollerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Drawable listDataChild;
    private final int LIST_LENGTH=10;

    public boolean toggleSwitchOn;

    public MockupScrollerAdapter(Drawable mockViewDrawable){
        listDataChild=mockViewDrawable;
    }

    public void updateDrawable(Drawable newDrawable){
        listDataChild=newDrawable;
        flipSwitch();
        //Just to make sure it animates
        notifyItemRangeChanged(0, LIST_LENGTH);
    }

    public static class MockViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv;

        public MockViewHolder(View v) {
            super(v);
            iv=(ImageView)v;
        }

        public ImageView getMockImageView() {
            return iv;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.mockup_list_item, parent, false);
        MockViewHolder viewHolder = new MockViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MockViewHolder vh = (MockViewHolder) holder;
        vh.getMockImageView().setImageDrawable(listDataChild);
    }

    @Override
    public int getItemCount() {
        return LIST_LENGTH;
    }

    private void flipSwitch(){
        toggleSwitchOn=!toggleSwitchOn;
    }
}
