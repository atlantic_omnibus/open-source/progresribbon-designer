/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner;

import android.content.Intent;
import android.os.Bundle;

import com.atlanticomnibus.ribbondesigner.core.ConfigSheetControl;
import com.atlanticomnibus.ribbondesigner.ui.about.AboutActivity;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.atlanticomnibus.ribbondesigner.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {


    /**
     * We need a reference to this, as it will be used in a callback
     */
    private ConfigSheetControl sheetControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar appbar = findViewById(R.id.toolbar);
        setSupportActionBar(appbar);

        /**
         * This be the main view's viewpager
         */
      SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        /**
         * The one menu item that is accessible everywhere. it leads to a pretty About page too!
         */
        if(item.getItemId()==R.id.about){
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * The interface's implementation here works both ways, when receiving the Ribbon created by the Fragment,
     * it is passed down to the newly created ConfigSheetControl class. This only happens when the fragment is
     * first created, so it's safe to assume ConfigSheetControl would not be instantiated yet. If configSheetControl
     * is not null, we can assume the ribbon was recreated through that (as there is no other way to recreate
     * the ribbon), in which case the fresh ribbon will be passed back to the Fragment.
     *
     * @param ribbonContainer the container the ribbon is initially attached to
     */
    public void onFragmentInitialised(ViewGroup ribbonContainer){
        if(sheetControl==null) {
            sheetControl = new ConfigSheetControl(this, findViewById(R.id.bottom_sheet_config_chooser), ribbonContainer);
        }
    }

}