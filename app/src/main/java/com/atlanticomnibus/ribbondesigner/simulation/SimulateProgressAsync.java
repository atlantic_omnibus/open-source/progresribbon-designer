/*
 * ProgressRibbon Designer v1.0
 *
 * Copyright (c) 2019 Attila Orosz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.atlanticomnibus.ribbondesigner.simulation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.atlanticomnibus.controlsheet.ControlSheet;
import com.atlanticomnibus.controlsheet.ControlStripButton;
import com.atlanticomnibus.progressribbon.ProgressRibbon;
import com.atlanticomnibus.ribbondesigner.R;
import com.atlanticomnibus.ribbondesigner.ui.main.MockupScrollerAdapter;
import com.atlanticomnibus.ribbondesigner.ui.main.ViewAnimations;
import com.atlanticomnibus.switchbutton.SwitchButton;

import java.util.concurrent.TimeUnit;


/**
 * Simulation of something happening in the background, so that the ProgressRibbon can show of its capabilities.
 */
public class SimulateProgressAsync extends AsyncTask<Boolean ,Integer, Void> {

    /*Service values*/
    private final int PROGRESS_FINISHED=-444,
                      HIDE_START_BUTTON=-555;

    private final int PROGRESS_SPEED_DELAY=50,
                      PROGRESS_BAR_START_SIMULATION_DELAY=500;

    /*Some views*/
    private Context mContext;
    private ProgressRibbon progressRibbon;
    private SwitchButton orphanModeToggle, blockParentToggle;
    private ControlStripButton playButton;
    MockupScrollerAdapter scrollerAdapter;

    /*Constructor to constructively construct*/
    public SimulateProgressAsync(Context context,
                                 ProgressRibbon progressRibbon,
                                 ControlSheet controlSheet,
                                 MockupScrollerAdapter scrollerAdapter){
        mContext=context;
        this.progressRibbon=progressRibbon;
        this.orphanModeToggle=controlSheet.findViewById(R.id.orphan_mode_toggle);
        this.blockParentToggle=controlSheet.findViewById(R.id.block_view_toggle);
        this.playButton = controlSheet.getControlStripButton(2);
        this.scrollerAdapter=scrollerAdapter;
    }

    /*Just a convenient way to pause for a little*/
    private void delay(int delayValue){
        try {
            TimeUnit.MILLISECONDS.sleep( delayValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Boolean... objects) {

        int initialDelay=PROGRESS_BAR_START_SIMULATION_DELAY + progressRibbon.getShowDelay();
        publishProgress(HIDE_START_BUTTON);

        if(progressRibbon.willAnimateAppearance()){
            initialDelay+=progressRibbon.getAnimationDuration()+1;
        }

        if(!objects[0]) {
            delay(initialDelay);
        }

        for(int i=1; i<progressRibbon.getMax()+1; i++){
            publishProgress(i);
            if(i==60) {// && progressRibbon.getProgressBarStyle()==ProgressRibbon.BAR_HORIZONTAL){
                for (int j=i; j<progressRibbon.getMax()+1; j++){
                    publishProgress(i, j);
                    if(progressRibbon.getProgressBarStyle()==ProgressRibbon.BAR_HORIZONTAL) {
                        delay(PROGRESS_SPEED_DELAY / 10);
                    } else {
                        delay(PROGRESS_SPEED_DELAY / 10);
                    }
                }
                publishProgress(i, 0);
            }
            delay(PROGRESS_SPEED_DELAY);
        }

        publishProgress(PROGRESS_FINISHED);


        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        if(values[0]==HIDE_START_BUTTON) {

            playButton.setEnabled(false);

        } else if(values[0]==PROGRESS_FINISHED){

            progressRibbon.setProgressText(R.string.progress_ended);
            progressRibbon.hide();

            if(progressRibbon.isInOrphanMode()){

                if(!blockParentToggle.isChecked()) {
                    orphanModeToggle.setEnabled(true);
                    blockParentToggle.setEnabled(true);
                }

            } else {
                orphanModeToggle.setEnabled(true);
            }

            playButton.setEnabled(true);


        } else if (!progressRibbon.isIndeterminate()) {

            if(values.length>1){
                progressRibbon.setSecondaryProgress(values[1]);
            } else {
                if (progressRibbon.getProgress() <= values[0]) {
                    progressRibbon.setProgress(values[0]);
                }

                String percent = "%";

                if (!progressRibbon.willReportProgressAsMaxPercent()) {
                    percent = "";
                }

                progressRibbon.setProgressText(String.format(mContext.getString(R.string.progress_bar_progress), progressRibbon.getProgress()) + percent);


                if (values[0] == progressRibbon.getMax() * 0.4) {
                    progressRibbon.freezeProgress();
                }

                if (values[0] == progressRibbon.getMax() * 0.5) {
                    progressRibbon.allowProgress();
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Drawable mockDrawable;
        int id;

        if(scrollerAdapter.toggleSwitchOn){
            id=R.drawable.mockup1;
        } else {
            id=R.drawable.mockup2;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mockDrawable=mContext.getDrawable(id);
        } else {
            mockDrawable=mContext.getResources().getDrawable(id);
        }

        scrollerAdapter.updateDrawable(mockDrawable);

    }

}
